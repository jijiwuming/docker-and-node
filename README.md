# docker-and-node

this is a dockerfile for building a docker container with docker and node, design to build and deploy myself project

## info

* node 8.11.4

## how to use the image

* It show as [jijiwuming/docker-and-node](https://hub.docker.com/r/jijiwuming/docker-and-node/)

```Dockerfile

FROM jijiwuming/docker-and-node:latest

```